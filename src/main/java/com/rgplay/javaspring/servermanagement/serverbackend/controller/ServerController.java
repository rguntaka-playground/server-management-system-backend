package com.rgplay.javaspring.servermanagement.serverbackend.controller;

import com.rgplay.javaspring.servermanagement.serverbackend.enumeration.Status;
import com.rgplay.javaspring.servermanagement.serverbackend.model.Response;
import com.rgplay.javaspring.servermanagement.serverbackend.model.Server;
import com.rgplay.javaspring.servermanagement.serverbackend.service.implementation.ServerServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@RestController
@RequiredArgsConstructor //this takes care of dependency injection of service variable
@RequestMapping("/server")
public class ServerController {
    private final ServerServiceImpl serverService;

    @GetMapping("/list")
    public ResponseEntity<Response> getServers() {
        return ResponseEntity.ok(Response.builder()
                .timeStamp(LocalDateTime.now())
                .data(Map.of("servers", serverService.findAll(30)))
                .message("Servers retrieved")
                .httpStatus(OK)
                .statusCode(OK.value())
                .build());
    }

    @GetMapping("/ping/{ipaddress}")
    public ResponseEntity<Response> ping(@PathVariable("ipAddress") String ipAddress) throws IOException {
        Server server = serverService.ping(ipAddress);
        return ResponseEntity.ok(Response.builder()
                .timeStamp(LocalDateTime.now())
                .data(Map.of("server", server))
                .message(server.getStatus() == Status.SERVER_UP ? "Ping Success" : "Ping failed")
                .httpStatus(OK)
                .statusCode(OK.value())
                .build());
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveServer(@RequestBody @Valid Server server) {
        return ResponseEntity.ok(Response.builder()
                .timeStamp(LocalDateTime.now())
                .data(Map.of("server", serverService.create(server)))
                .message("Server created")
                .httpStatus(OK)
                .statusCode(OK.value())
                .build());
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Response> getServer(@PathVariable("id") Long id) {
        return ResponseEntity.ok(Response.builder()
                .timeStamp(LocalDateTime.now())
                .data(Map.of("server", serverService.get(id)))
                .message("Server fetched")
                .httpStatus(OK)
                .statusCode(OK.value())
                .build());

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Response> deleteServer(@PathVariable("id") Long id) {
        return ResponseEntity.ok(Response.builder()
                .timeStamp(LocalDateTime.now())
                .data(Map.of("deleted", serverService.delete(id)))
                .message("Server deleted")
                .httpStatus(OK)
                .statusCode(OK.value())
                .build());

    }


    /*
    For all the above methods, they deal with JSON, hence, not required to explicitly mention produces.
    And as there is no need of additional variable, default variable path need not be mentioned explicitly.

    But, in this case, as we have to mention additional variable, its essential to mention 'path'
     */
    @GetMapping(path="/image/{fileName}", produces = IMAGE_PNG_VALUE)
    public byte[] getServerImage(@PathVariable("fileName") String fileName) throws IOException{
        return Files.readAllBytes(Paths.get(System.getProperty("user.home") + "Downloads/images/"+ fileName));

    }
}
