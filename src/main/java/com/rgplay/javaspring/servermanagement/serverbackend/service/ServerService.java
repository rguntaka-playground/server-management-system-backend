package com.rgplay.javaspring.servermanagement.serverbackend.service;

import com.rgplay.javaspring.servermanagement.serverbackend.model.Server;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collection;

public interface ServerService {
    Server create(Server server);
    Server ping(String ipAddress) throws IOException;
    Collection<Server> findAll(int limit);
    Server get(Long id);
    Server update(Server server);
    Boolean delete(Long id);

}
