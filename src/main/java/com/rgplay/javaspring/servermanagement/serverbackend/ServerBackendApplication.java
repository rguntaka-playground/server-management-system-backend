package com.rgplay.javaspring.servermanagement.serverbackend;

import com.rgplay.javaspring.servermanagement.serverbackend.enumeration.Status;
import com.rgplay.javaspring.servermanagement.serverbackend.model.Server;
import com.rgplay.javaspring.servermanagement.serverbackend.repository.ServerRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerBackendApplication.class, args);
    }

    CommandLineRunner run(ServerRepo serverRepo) {
        return args -> {
            serverRepo.save(new Server(null,
                    "192.168,1,168",
                    "Ubuntu Linux",
                    "16 GB",
                    "Web Server",
                    "http:localhost:8080/server/image/server_image1.png",
                    Status.SERVER_UP));
            serverRepo.save(new Server(null,
                    "192.168,1,68",
                    "Windows",
                    "8 GB",
                    "Personal PC",
                    "http:localhost:8080/server/image/server_image2.png",
                    Status.SERVER_UP));
        };
    }
}
