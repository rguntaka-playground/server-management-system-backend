package com.rgplay.javaspring.servermanagement.serverbackend.model;

import com.rgplay.javaspring.servermanagement.serverbackend.enumeration.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Server {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique=true)
    @NotEmpty(message = "IP Address can't be empty")
    private String ipAddress; //ipaddress should be unique as No two servers should have the same ipaddress
    private String name;
    private String memory;
    private String type;
    private String imageUrl;
    private Status status;
}
