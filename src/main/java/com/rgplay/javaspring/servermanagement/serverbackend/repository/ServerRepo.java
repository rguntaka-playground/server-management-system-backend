package com.rgplay.javaspring.servermanagement.serverbackend.repository;

import com.rgplay.javaspring.servermanagement.serverbackend.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServerRepo extends JpaRepository<Server, Long> {
    Server findByIpAddress(String ipAddress);
}
