package com.rgplay.javaspring.servermanagement.serverbackend.service.implementation;

import com.rgplay.javaspring.servermanagement.serverbackend.enumeration.Status;
import com.rgplay.javaspring.servermanagement.serverbackend.model.Server;
import com.rgplay.javaspring.servermanagement.serverbackend.repository.ServerRepo;
import com.rgplay.javaspring.servermanagement.serverbackend.service.ServerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Random;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class ServerServiceImpl implements ServerService {

    @Autowired
    private final ServerRepo serverRepo;

    @Override
    public Server create(Server server) {
        log.info("ServerServiceImpl: create(server): "+server.getName());
        server.setImageUrl(setServerImageUrl());
        return serverRepo.save(server);
    }


    @Override
    public Server ping(String ipAddress) throws IOException {
        log.info("ServerServiceImpl: ping(ipaddress): "+ipAddress);
        Server server = serverRepo.findByIpAddress(ipAddress);
        InetAddress inetAddress = InetAddress.getByName(ipAddress);
        server.setStatus(inetAddress.isReachable(10000)?Status.SERVER_UP: Status.SERVER_DOWN);
        serverRepo.save(server); //here the app will literally save the server status fetched above
        return server;
    }

    @Override
    public Collection<Server> findAll(int limit) {
        log.info("ServerServiceImpl: findAll: "+limit);
        return serverRepo.findAll(PageRequest.of(0,limit)).toList();
    }

    @Override
    public Server get(Long id) {
        log.info("ServerServiceImpl: get: ");
        return serverRepo.findById(id).get();
    }

    @Override
    public Server update(Server server) {
        log.info("ServerServiceImpl: update: ");
        return serverRepo.save(server); //Jpa will create new server if the id doesn't exist, else, it will update the existing one
    }

    @Override
    public Boolean delete(Long id) {
        log.info("ServerServiceImpl: delete: ");
        serverRepo.delete(serverRepo.findById(id).get());
        return Boolean.TRUE;
    }


    private static String setServerImageUrl() {
        String[] imageNames = {"server_image1.png", "server_image2.png", "server_image3.png", "server_image4.png"};
        return ServletUriComponentsBuilder.fromCurrentContextPath().path("server/images/" + imageNames[new Random().nextInt(4)]).toUriString();
    }

}
